<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>FORMULAIRE</title>
</head>
<body>
<div><label for="nom">Nom *</label>
<input type="text" id="nom" name="nom" value=""/>
<span style='color: red'>${form.erreurs['nom']}</span>
</div>
<div><label for="prenom">Prénom *</label>
<input type="text" id= "prénom" name="prenom" value=""/>
<span style='color: red'>${form.erreurs['prenom']}</span>
</div>
<div><label for="telephone">Téléphone *</label>
<input type="text" id= "téléphone" name="telephone" value=""/>
<span style='color: red'>${form.erreurs['telephone']}</span>
</div>
<div><label for="email">@Email *</label>
<input type="email" id= "email" name="email" value=""/>
<span style='color: red'>${form.erreurs['email']}</span>
</div>
<!-- <c:url value="/CreationClient" var="monLien" />  -->
<!-- <c:url value="/CreationAdresse" var="monLien" />  -->
</body>
</html>