<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Affichage d'une adresse</title>
</head>
<body>
 <%-- ${adresse.ville}  ${adresse.rue}  ${adresse.codePostal}  
 ${client.nom} ${adresse.client.prenom} ${adresse.client.telephone} ${adresse.client.email}  --%>
<%--  <p class="info">${ message }</p> --%>
 <c:import url="/INC/menu.jsp"/>
<c:out value="${adresse.ville}" />
<br/>
<c:out value="${adresse.rue}" />
<br/>
<c:out value="${adresse.codePostal}" />
<br/>
<c:out value="${adresse.client.nom}" />
<br/>
<c:out value="${adresse.client.prenom}" />
<br/>
<c:out value="${adresse.client.telephone}" />
<br/>
<c:out value="${adresse.client.email}" />
<br/>
<p><c:out value= "${ message }" escapeXml="false"/></p> 
</body>
</html>