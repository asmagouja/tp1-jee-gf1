<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Import de mettre ce taglib si on utilise jstl -->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Création d'une adresse</title>
</head>
<body>

	<form method="post" action="creationAdresse">
		<div>Formulaire de création d'une commande</div>
		<div>
			<label for="rue">Rue *</label> <input type="text" id="rue" name="rue"
				value="" /> <span style='color: red'>${form.erreurs['rue']}</span>
		</div>
		<div>
			<label for="ville">Ville *</label> <input type="text" id="ville"
				name="ville" value="" /><span style='color: red'>${form.erreurs['ville']}</span>
		</div>
		<div>
			<label for="codePostal">CodePostal *</label> <input type="text"
				id="codePostal" name="codePostal" value="" /><span style='color: red'>${form.erreurs['codePostal']}</span>
		</div>
		<c:import url="/INC/inc_client_form.jsp" />
		<input type="submit" value="Enregistrer" />
	</form>
	<!-- <div>Données du cient</div> -->
	<!-- <div><label for="nom">Nom *</label>
<input type="text" id="nom" name="nom" value=""/>
</div>
<div><label for="prenom">Prénom *</label>
<input type="text" id= "prénom" name="prenom" value=""/>
</div>
<div><label for="telephone">Téléphone *</label>
<input type="text" id= "téléphone" name="telephone" value=""/>
</div>
<div><label for="email">@Email *</label>
<input type="email" id= "email" name="email" value=""/>
</div>
<input type="submit" value="Enregistrer"/>-->
	<c:import url="/INC/menu.jsp" />
</body>
</html>