<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Affichage d'un client</title>
</head>
<body>
<%-- ${client.prenom}  ${client.nom}  ${client.telephone}  ${client.email}  
<p class="info">${ message }</p> --%>
<c:import url="/INC/menu.jsp"/> 
<br/>
<c:out value="${client.nom}" />
<br/>
<c:out value="${client.prenom}" />
<br/>
<c:out value="${client.telephone}" />
<br/>
<c:out value="${client.email}" />
<br/>
<p><c:out value= "${ message }" escapeXml="false"/></p> 
</body>
</html>