package org.eclipse.form.validator;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;

import org.eclipse.beans.Adresse;
import org.eclipse.beans.Client;

/**** méthode 3 validation 05/02/20 ****/

public class AjouterAdresseForm {
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_PRENOM = "prenom";
	private static final String CHAMP_TELEPHONE = "telephone";
	private static final String CHAMP_EMAIL = "email";
	private static final String CHAMP_RUE = "rue";
	private static final String CHAMP_VILLE = "ville";
	private static final String CHAMP_CODE_POSTAL = "codePostal";
	private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern
			.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	private static final Pattern VALID_TELEPHONE_REGEX = Pattern
			.compile("\\d{10}+", Pattern.CASE_INSENSITIVE);
	private static final Pattern VALID_CODE_POSTAL_REGEX = Pattern
			.compile("\\d{4}+", Pattern.CASE_INSENSITIVE);
	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();
	
	public Map<String, String> getErreurs() {
		return erreurs;
	}

	public String getResultat() {
		return resultat;
	}

	public Adresse creerAdresse(HttpServletRequest request) {
		String nom = getValeurChamp(request, CHAMP_NOM);
		String prenom = getValeurChamp(request, CHAMP_PRENOM);
		String telephone = getValeurChamp(request, CHAMP_TELEPHONE);
		String email = getValeurChamp(request, CHAMP_EMAIL);
		String rue = getValeurChamp(request, CHAMP_RUE);
		String ville = getValeurChamp(request, CHAMP_VILLE);
		String codePostal = getValeurChamp(request, CHAMP_CODE_POSTAL);
		Client client = new Client();
		Adresse adresse = new Adresse();
		try {
			validationNom(nom);
			client.setNom(nom);
		} catch (Exception e) {
			setErreur(CHAMP_NOM, e.getMessage());
		}
		try {
			validationPrenom(prenom);
			client.setPrenom(prenom);
		} catch (Exception e) {
			setErreur(CHAMP_PRENOM, e.getMessage());
		}
		try {
			validationEmail(email);
			client.setEmail(email);
		} catch (Exception e) {
			setErreur(CHAMP_EMAIL, e.getMessage());
		}
		try {
			validationTelephone(telephone);
			client.setTelephone(telephone);
		} catch (Exception e) {
			setErreur(CHAMP_TELEPHONE, e.getMessage());
		}
		try {
			validationRue(rue);
			adresse.setRue(rue);
		} catch (Exception e) {
			setErreur(CHAMP_RUE, e.getMessage());
		}
		try {
			validationVille(ville);
			adresse.setVille(ville);
		} catch (Exception e) {
			setErreur(CHAMP_VILLE, e.getMessage());
		}
		try {
			validationCodePostal(codePostal);
			adresse.setCodePostal(codePostal);
		} catch (Exception e) {
			setErreur(CHAMP_CODE_POSTAL, e.getMessage());
		}
		adresse.setClient(client);
		if (erreurs.isEmpty()) {
			resultat = "Succès de la création de l'adresse.";
		} else {
			resultat = "Échec de la création de l'adresse.";
		}
		return adresse;
	}
	
	

	private void validationNom(String nom) throws Exception {
		if (nom != null) {
			if (nom.length() < 2) {
				throw new Exception("Le nom d'utilisateur doit contenir au moins 2 caractères.");
			}
		} else {
			throw new Exception("Merci d'entrer un nom d'utilisateur.");
		}
	}

	private void validationPrenom(String prenom) throws Exception {
		if (prenom != null) {
			if (prenom.length() < 2) {
				throw new Exception("Le nom d'utilisateur doit contenir au moins 2 caractères.");
			}
		} else {
			throw new Exception("Merci d'entrer un nom d'utilisateur.");
		}
	}
	
	private void validationVille(String ville) throws Exception {
		if (ville != null) {
			if (ville.length() < 2) {
				throw new Exception("La ville doit contenir au moins 2 caractères.");
			}
		} else {
			throw new Exception("Merci d'entrer une ville");
		}
	}
	
	private void validationRue(String rue) throws Exception {
		if (rue != null) {
			if (rue.length() < 2) {
				throw new Exception("La rue doit contenir au moins 2 caractères.");
			}
		} else {
			throw new Exception("Merci d'entrer une rue");
		}
	}

	public static void validationEmail(String email) throws Exception {
		if(email != null) {
			Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
			boolean result = matcher.find();
			if(!result) {
				throw new Exception("L'email n'est pas valide");
			}
		}
		else {
			throw new Exception("Merci d'entrer un email.");
		}
	}
	
	public static void validationTelephone(String telephone) throws Exception {
		if(telephone != null) {
			Matcher matcher = VALID_TELEPHONE_REGEX.matcher(telephone);
			boolean result = matcher.find();
			if(!result) {
				throw new Exception("Le numéro de téléphone n'est pas valide");
			}
		}
		else {
			throw new Exception("Merci d'entrer un numéro de téléphone");
		}
	}
	
	public static void validationCodePostal(String codePostal) throws Exception {
		if(codePostal != null) {
			Matcher matcher = VALID_CODE_POSTAL_REGEX.matcher(codePostal);
			boolean result = matcher.find();
			if(!result) {
				throw new Exception("Le code postal n'est pas valide");
			}
		}
		else {
			throw new Exception("Merci d'entrer un code postal");
		}
	}

	
	/*
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	/*
	 * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur;
		}
	}
}
