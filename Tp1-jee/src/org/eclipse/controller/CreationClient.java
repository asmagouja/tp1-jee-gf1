package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.beans.Adresse;
import org.eclipse.beans.Client;
import org.eclipse.dao.AdresseDaoImpl;
import org.eclipse.dao.ClientDaoImpl;
import org.eclipse.dao.Dao;
import org.eclipse.form.validator.AjouterClientForm;

/**
 * Servlet implementation class CreationClient
 */
@WebServlet("/creationClient")
public class CreationClient extends HttpServlet {

	public static final String ATT_CLIENT = "client";
	public static final String ATT_FORM = "form";

	public static final String CREER_CLIENT = "/WEB-INF/creerClient.jsp";
	public static final String AFFICHER_CLIENT = "/WEB-INF/afficherClient.jsp";
	private static final long serialVersionUID = 1L;
	
	public CreationClient() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(CREER_CLIENT).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AjouterClientForm form = new AjouterClientForm();
		Client client = form.creerClient(request);
		request.setAttribute(ATT_FORM, form);
		if (form.getErreurs().isEmpty()) {
			Dao<Client> clientDao = new ClientDaoImpl();
			client = clientDao.save(client);
			request.setAttribute(ATT_CLIENT, client);
			/* Si aucune erreur, alors affichage de la fiche r�capitulative */
			this.getServletContext().getRequestDispatcher(AFFICHER_CLIENT).forward(request, response);
		} else {
			/* Sinon, r�-affichage du formulaire de cr�ation avec les erreurs */
			this.getServletContext().getRequestDispatcher(CREER_CLIENT).forward(request, response);
		}
	}
}
