package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.beans.Adresse;
import org.eclipse.beans.Client;
import org.eclipse.dao.AdresseDaoImpl;
import org.eclipse.dao.Dao;
import org.eclipse.form.validator.AjouterAdresseForm;
import org.eclipse.form.validator.AjouterClientForm;

/**
 * Servlet implementation class CreationAdresse
 */
@WebServlet("/creationAdresse")
public class CreationAdresse extends HttpServlet {

	public static final String ATT_ADRESSE = "adresse";
	public static final String ATT_FORM = "form";

	public static final String CREER_ADRESSE = "/WEB-INF/creerAdresse.jsp";
	public static final String AFFICHER_ADRESSE = "/WEB-INF/afficherAdresse.jsp";

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreationAdresse() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(CREER_ADRESSE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AjouterAdresseForm form = new AjouterAdresseForm();
		Adresse adresse = form.creerAdresse(request);
		System.out.println(form.getErreurs());
		request.setAttribute(ATT_FORM, form);
		if (form.getErreurs().isEmpty()) {
			/*
			 * Si aucune erreur, alors on enregistre en base et on passe � l'affichage de la
			 * fiche r�capitulative
			 */
			Dao<Adresse> adresseDao = new AdresseDaoImpl();
			adresse = adresseDao.save(adresse);
			request.setAttribute(ATT_ADRESSE, adresse);

			this.getServletContext().getRequestDispatcher(AFFICHER_ADRESSE).forward(request, response);
		} else {
			/* Sinon, r�-affichage du formulaire de cr�ation avec les erreurs */
			this.getServletContext().getRequestDispatcher(CREER_ADRESSE).forward(request, response);
		}
	}
}
