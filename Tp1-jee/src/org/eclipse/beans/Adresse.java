package org.eclipse.beans;

public class Adresse {
	private int id;
	private String rue;
	private String ville;
	private String codePostal;

	private Client client;

	public Adresse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Adresse(String rue, String ville, String codePostal, Client client) {
		super();
		this.rue = rue;
		this.ville = ville;
		this.codePostal = codePostal;
		this.client = client;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
