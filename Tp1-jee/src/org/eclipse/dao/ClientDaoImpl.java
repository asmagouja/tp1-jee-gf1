package org.eclipse.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.eclipse.beans.Client;

public class ClientDaoImpl implements Dao<Client>{

	@Override
	public Client save(Client client) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				c.setAutoCommit(false);
				PreparedStatement ps = c.prepareStatement("insert into client (nom,prenom,telephone,email) values (?,?,?,?); ",
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, client.getNom());
				ps.setString(2, client.getPrenom());
				ps.setString(3, client.getTelephone());
				ps.setString(4, client.getEmail());
				ps.executeUpdate();
				ResultSet resultat = ps.getGeneratedKeys();
				if (resultat.next()) {
					client.setId(resultat.getInt(1));
				}
				c.commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return client;
	}

}
