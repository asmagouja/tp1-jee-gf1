package org.eclipse.dao;

public interface Dao<T> {
	T save(T o);
}
