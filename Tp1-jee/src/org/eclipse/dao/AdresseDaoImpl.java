package org.eclipse.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.eclipse.beans.Adresse;
import org.eclipse.beans.Client;

public class AdresseDaoImpl implements Dao<Adresse> {

	@Override
	public Adresse save(Adresse adresse) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				Dao<Client> clientDao = new ClientDaoImpl();
				Client client = clientDao.save(adresse.getClient());
				Class.forName("com.mysql.cj.jdbc.Driver");
				c.setAutoCommit(false);
				PreparedStatement ps = c.prepareStatement("insert into adresse (rue,ville,codePostal,client_id) values (?,?,?,?); ",
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, adresse.getRue());
				ps.setString(2, adresse.getVille());
				ps.setString(3, adresse.getCodePostal());
				ps.setInt(4, client.getId());
				ps.executeUpdate();
				ResultSet resultat = ps.getGeneratedKeys();
				if (resultat.next()) {
					adresse.setId(resultat.getInt(1));
				}
				c.commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return adresse;
	}

}
